package server

import (
	"errors"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
)

func RunServer() {
	r := gin.Default()
	r.GET("/parse", func(c *gin.Context) {
		_, err := downloadHtml()
		if err != nil {
			c.JSON(401, gin.H{
				"message": "something gone bad",
			})
		} else {
			c.JSON(200, gin.H{
				"message": "all done!",
			})
		}

	})

	r.Run()
}

func downloadHtml() (int, error) {
	res, err := http.Get("http://wahapedia.ru/wh40k8ed/factions/tau-empire/datasheets.html")
	if err != nil {
		return -1, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return -1, errors.New("some problem happened with request")
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return -1, err
	}
	err = ioutil.WriteFile("text.txt", body, 0644)
	if err != nil {
		return -1, err
	} else {
		return 0, nil
	}
}
