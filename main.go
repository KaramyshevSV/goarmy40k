package main

import (
	"bitbucket.org/KaramyshevSV/goarmy40k/server"
)

func main() {
	server.RunServer()
}
